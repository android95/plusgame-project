package com.example.game

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_game.*
import kotlin.random.Random


class GameActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_game)

        fun Play() {


            val random1: Int = Random.nextInt(10) + 1
            textView.setText(Integer.toString(random1))

            val random2: Int = Random.nextInt(10) + 1
            textView2.setText(Integer.toString(random2))

            val sum = random1 + random2

            val position: Int = Random.nextInt(3) + 1

            if (position == 1) {
                btnButton1.setText(Integer.toString(sum))
                btnButton2.setText(Integer.toString(sum - 2))
                btnButton3.setText(Integer.toString(sum + 2))
            } else if (position == 2) {
                btnButton2.setText(Integer.toString(sum))
                btnButton1.setText(Integer.toString(sum - 2))
                btnButton3.setText(Integer.toString(sum + 2))
            } else {
                btnButton3.setText(Integer.toString(sum))
                btnButton2.setText(Integer.toString(sum - 2))
                btnButton1.setText(Integer.toString(sum + 2))
            }
            btnButton1.setOnClickListener {
                if (btnButton1.text.toString().toInt() == sum) {
                    result.setText("Correct")
                    amountCorrect.text = (amountCorrect.text.toString().toInt() + 1).toString()

                } else {
                    result.setText("Wrong")
                    amountWrong.text = (amountCorrect.text.toString().toInt() + 1).toString()
                }
            }
            btnButton2.setOnClickListener {
                if (btnButton2.text.toString().toInt() == sum) {
                    result.setText("Correct")
                    amountCorrect.text = (amountCorrect.text.toString().toInt() + 1).toString()

                } else {
                    result.setText("Wrong")
                    amountWrong.text = (amountCorrect.text.toString().toInt() + 1).toString()
                }
            }
            btnButton3.setOnClickListener {
                if (btnButton3.text.toString().toInt() == sum) {
                    result.setText("Correct")
                    amountCorrect.text = (amountCorrect.text.toString().toInt() + 1).toString()

                } else {
                    result.setText("Wrong")
                    amountWrong.text = (amountCorrect.text.toString().toInt() + 1).toString()
                }
            }
        }

        Play()
           btnNext.setOnClickListener{
               result.setText("Please Select an Answer")
               Play()
           }
    }
}