package com.example.game

import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.ImageButton
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    private val REQUEST_MAIN = 11
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val btnStart = findViewById<ImageButton>(R.id.btnStart)
        btnStart.setOnClickListener {
            val intent = Intent(MainActivity@ this, GameActivity::class.java)
            //startActivity(intent)
        }

        val btnMultiply = findViewById<ImageButton>(R.id.btnMultiply)
        btnMultiply.setOnClickListener {
            val intent = Intent(MultiplyActivity@ this, MultiplyActivity::class.java)
            //startActivity(intent)
        }

        val btnMinus = findViewById<ImageButton>(R.id.btnMinus)
        btnMinus.setOnClickListener {
            val intent = Intent(MinusActivity@ this, MinusActivity::class.java)
            intent.putExtra("correct2", scoreCorrectMinusMain.text);
            intent.putExtra("wrong2", scoreWrongMinusMain.text);
            startActivityForResult(intent, REQUEST_MAIN)

            //startActivity(intent)
        }
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
            super.onActivityResult(requestCode, resultCode, data)
            if (requestCode == REQUEST_MAIN) {
                if (resultCode == Activity.RESULT_OK)
                    scoreCorrectMinusMain.text =  data?.getStringExtra("correct2")
                    scoreWrongMinusMain.text =  data?.getStringExtra("wrong2")

                } else if (resultCode == Activity.RESULT_CANCELED) {
                    //TODO Handle Result Cancel
                }
            }

        }




