package com.example.game

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_game.*
import kotlinx.android.synthetic.main.activity_multiply.*
import kotlin.random.Random

class MultiplyActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_multiply)

        fun Play() {
            val random1: Int = Random.nextInt(10) + 1
            textViewMul1.setText(Integer.toString(random1))

            val random2: Int = Random.nextInt(10) + 1
            textViewMul2.setText(Integer.toString(random2))

            val sum = random1 * random2

            val position: Int = Random.nextInt(3) + 1


            if (position == 1) {
                btnButtonMul1.setText(Integer.toString(sum))
                btnButtonMul2.setText(Integer.toString(sum - 2))
                btnButtonMul3.setText(Integer.toString(sum + 2))
            } else if (position == 2) {
                btnButtonMul2.setText(Integer.toString(sum))
                btnButtonMul1.setText(Integer.toString(sum - 2))
                btnButtonMul3.setText(Integer.toString(sum + 2))
            } else {
                btnButtonMul3.setText(Integer.toString(sum))
                btnButtonMul2.setText(Integer.toString(sum - 2))
                btnButtonMul1.setText(Integer.toString(sum + 2))
            }
            btnButtonMul1.setOnClickListener {
                if (btnButtonMul1.text.toString().toInt() == sum) {
                    result2.setText("Correct")
                    amountCorrectMul.text = (amountCorrectMul.text.toString().toInt() + 1).toString()

                } else {
                    result2.setText("Wrong")
                    amountWrongMul.text = (amountWrongMul.text.toString().toInt() + 1).toString()
                }
            }
            btnButtonMul2.setOnClickListener {
                if (btnButtonMul2.text.toString().toInt() == sum) {
                    result2.setText("Correct")
                    amountCorrectMul.text = (amountCorrectMul.text.toString().toInt() + 1).toString()

                } else {
                    result2.setText("Wrong")
                    amountWrongMul.text = (amountWrongMul.text.toString().toInt() + 1).toString()
                }
            }
            btnButtonMul3.setOnClickListener {
                if (btnButtonMul3.text.toString().toInt() == sum) {
                    result2.setText("Correct")
                    amountCorrectMul.text =
                        (amountCorrectMul.text.toString().toInt() + 1).toString()

                } else {
                    result2.setText("Wrong")
                    amountWrongMul.text = (amountWrongMul.text.toString().toInt() + 1).toString()
                }
            }
        }
        Play()
        btnNextMul.setOnClickListener{
            result2.setText("Please Select an Answer")
            Play()
        }
    }
}