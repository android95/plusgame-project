package com.example.game

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView
import kotlinx.android.synthetic.main.activity_game.*
import kotlinx.android.synthetic.main.activity_minus.*
import kotlinx.android.synthetic.main.activity_multiply.*
import kotlin.random.Random

class MinusActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_minus)

        fun Play() {
            val random1: Int = Random.nextInt(10) + 1
            textViewMinus1.setText(Integer.toString(random1))

            val random2: Int = Random.nextInt(10) + 1
            textViewMinus2.setText(Integer.toString(random2))

            val sum = random1 - random2

            val position: Int = Random.nextInt(3) + 1

            if (position == 1) {
                btnButtonMinus1.setText(Integer.toString(sum))
                btnButtonMinus2.setText(Integer.toString(sum - 2))
                btnButtonMinus3.setText(Integer.toString(sum + 2))
            } else if (position == 2) {
                btnButtonMinus2.setText(Integer.toString(sum))
                btnButtonMinus1.setText(Integer.toString(sum - 2))
                btnButtonMinus3.setText(Integer.toString(sum + 2))
            } else {
                btnButtonMul3.setText(Integer.toString(sum))
                btnButtonMinus2.setText(Integer.toString(sum - 2))
                btnButtonMinus3.setText(Integer.toString(sum + 2))
            }

            btnButtonMinus1.setOnClickListener {
                if (btnButtonMinus1.text.toString().toInt() == sum) {
                    result3.setText("Correct")
                    amountCorrectMinus.text =
                        (amountCorrectMinus.text.toString().toInt() + 1).toString()

                } else {
                    result3.setText("Wrong")
                    amountWrongMinus.text =
                        (amountWrongMinus.text.toString().toInt() + 1).toString()
                }
            }
            btnButtonMinus2.setOnClickListener {
                if (btnButtonMinus2.text.toString().toInt() == sum) {
                    result3.setText("Correct")
                    amountCorrectMinus.text =
                        (amountCorrectMinus.text.toString().toInt() + 1).toString()

                } else {
                    result3.setText("Wrong")
                    amountWrongMinus.text =
                        (amountWrongMinus.text.toString().toInt() + 1).toString()
                }
            }
            btnButtonMinus3.setOnClickListener {
                if (btnButtonMinus3.text.toString().toInt() == sum) {
                    result3.setText("Correct")
                    amountCorrectMinus.text =
                        (amountCorrectMinus.text.toString().toInt() + 1).toString()

                } else {
                    result3.setText("Wrong")
                    amountWrongMinus.text =
                        (amountWrongMinus.text.toString().toInt() + 1).toString()
                }
            }
        }
        Play()
        btnNextMinus.setOnClickListener{
            result3.setText("Please Select an Answer")
            Play()
        }
        btnHomeMinus.setOnClickListener{
            val amountCorrectMinus = findViewById<TextView>(R.id.amountCorrectMinus)
            val amountWrongMinus = findViewById<TextView>(R.id.amountWrongMinus)
            val intent = Intent()
            intent.putExtra("correct2", amountCorrectMinus.text);
            intent.putExtra("wrong2", amountWrongMinus.text);
            setResult(RESULT_OK, intent);
            finish();

        }
    }
}